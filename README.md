# EMS API Connector
Imports data from [DEA’s EMS](https://www.emssoftware.com/) application's SOAP API interface into nodes/ taxonomy on Drupal.

This module requires the [EMS Event](https://www.drupal.org/sandbox/wingmanjd/2773017) module.

## Configuration
After enabling both ems_api_connector and ems_booking modules, the configuration page is located at /admin/config/services/ems_api_settings/ .
To import content, visit /admin/content/ems_api_connector_import, or click on the admin menu item under Content menu dropdown. 

## Creating Calendars
The first import will create the necessary taxonomy terms to create Calendar Logic taxonomy terms (/admin/structure/taxonomy/ems_calendar_logic).  Fill in the appropriate filters and run another import to retrieve all bookings

Manual calendar entries (non-EMS bookings) can be created by entering data into the typical /node/add/ems-booking Drupal form.

## Viewing Calendars
Calendar views are visible at /calendar, /calendar/day, /calendar/week, /calendar/month, /calendar/year.

## Todo
There's a lot of work still left for this project.  
* It would be nice to add additional fields (images, file uploads, URL fields, etc.) to the content type to allow augmentation of event data.
* Lock EMS data fields from being edited if the data came from EMS.  EMS should be considered the canonical source.
* Auto update an individual booking EMS data on node_save if the node was updated by a Drupal user.
* Add R/W capability from Drupal back to EMS.  This module was initially written with read-only EMS API access in mind, although DEA EMS does allow for R/W if the license is purchased.
