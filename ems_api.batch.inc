<?php

/**
 * @file
 * Performs batch processes for EMS API module.
 */

include_once 'plugins/ems_api_call.php';
include_once 'plugins/ems_get_bookings.php';
include_once 'plugins/ems_get_statuses.php';
include_once 'plugins/ems_get_group_types.php';
include_once 'plugins/ems_get_event_types.php';
// todo: coallate XML parsing into API object?
/**
 * EMS API batch process.
 */
function ems_api_batch_import(&$context) {
  // Sets batch_limit to a time interval of 1 week, in seconds.
  $batch_limit = 60 * 60 * 24 * 7;
  if (empty($context['sandbox'])) {
    // Initialize sandbox and clear database tables.
    $context['sandbox'] = array();
    db_delete('ems_api_all_bookings_result')->execute();

    // Set initial import time window based on values from configuration form.
    $context['sandbox']['progress'] = ems_api_convert_import_date(variable_get('ems_beginning_import_date_window'), -1);
    $context['results']['start'] = ems_api_convert_import_date(variable_get('ems_beginning_import_date_window'), -1);
    $context['results']['total_bookings'] = 0;
    $context['sandbox']['max'] = ems_api_convert_import_date(variable_get('ems_end_import_date_window'), 1);

    // Perform import of taxonomy terms from EMS.
    ems_api_eventtype_import();
    ems_api_grouptype_import();
    ems_api_statusid_import();
  }
  $message = "Importing " . $context['sandbox']['progress'] . " to " . date('Y-m-d', (strtotime($context['sandbox']['progress']) + $batch_limit));
  watchdog('EMS API', $message);
  // Import bookings for current batch time interval.
  $context['results']['bookings'] = ems_api_bookings_import(
    $context['sandbox']['progress'],
    date('Y-m-d', (strtotime($context['sandbox']['progress']) + $batch_limit))
  );
  // Process results.
  foreach ($context['results']['bookings'] as $ems_booking) {
    if (ems_api_export_to_db($ems_booking) === NULL) {
      $context['success'] = NULL;
    }

    // Increment total number of events processed.
    $context['results']['total_bookings']++;
  }

  // Increment time window.
  $context['sandbox']['progress'] = date('Y-m-d', (strtotime($context['sandbox']['progress']) + $batch_limit));
  // Check if the batch is finished.
  if (strtotime($context['sandbox']['progress']) < strtotime($context['sandbox']['max'])) {
    $context['finished'] = ((strtotime($context['sandbox']['progress']) - strtotime($context['results']['start'])) / (strtotime($context['sandbox']['max']) - strtotime($context['results']['start'])));
  }
}

/**
 * Performs CrUD on nodes.
 *
 * @param array $context
 *   Batch context array.
 */
function ems_api_batch_process(&$context) {
  $batch_limit = 50;
  $query = db_select('ems_api_all_bookings_result', 'e');
  if (empty($context['sandbox'])) {
    // Initialize sandbox.
    $context['sandbox'] = array();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results']['nodes_saved'] = 0;
    $context['results']['nodes_deleted'] = 0;
    // todo: Create/ Update New Groups
    // todo: Need to remove events that have a booking ID but aren't
    // in the feed.
  }
  // Fetch $batch_limit records from $query result table.
  $results = $query
    ->fields('e')
    ->orderBy('BookingID')
    ->range($context['sandbox']['progress'], $batch_limit)
    ->execute();

  // Process each booking to a Drupal node
  // of type ems_event based on calendar logic.
  while ($booking = $results->fetchAssoc()) {
    $node = new stdClass();
    $node->type = 'ems_booking';
    $node->uid = 1;

    $existing_node_id = ems_api_check_booking_exists($booking['BookingID']);
    // If found, load existing node.
    if ($existing_node_id) {
      $node = node_load($existing_node_id);
      // Delete each calendar logic to recalculate later.
      foreach ($node->field_ems_calendar_logic[LANGUAGE_NONE] as $key => $calendar) {
        unset($node->field_ems_calendar_logic[LANGUAGE_NONE][$key]);
      }
    }
    $booking_status_tid = get_tid_from_description($booking['StatusID'], 'ems_statuses');
    $booking_event_type_tid = get_tid_from_description($booking['EventTypeID'], 'ems_booking_types');
    $booking_group_type_tid = get_tid_from_description($booking['GroupTypeID'], 'ems_group_types');

    // Check logic of each ems_calendar_logic term.
    if ($vocabulary = taxonomy_vocabulary_machine_name_load('ems_calendar_logic')) {
      foreach (taxonomy_get_tree($vocabulary->vid) as $calendar_logic) {
        $status_results = NULL;
        $event_type_results = NULL;
        $group_type_results = NULL;

        $status_query = db_select('field_data_field_ems_status', 's')
          ->fields('s')
          ->condition('s.entity_id', $calendar_logic->tid, '=');
        $event_type_query = db_select('field_data_field_ems_booking_type', 'e')
          ->fields('e')
          ->condition('e.entity_id', $calendar_logic->tid, '=');
        $group_type_query = db_select('field_data_field_ems_group_type', 'g')
          ->fields('g')
          ->condition('g.entity_id', $calendar_logic->tid, '=');

        $status_count = $status_query
          ->countQuery()
          ->execute()
          ->fetchField();
        $event_type_count = $event_type_query
          ->countQuery()
          ->execute()
          ->fetchField();
        $group_type_count = $group_type_query
          ->countQuery()
          ->execute()
          ->fetchField();

        if ($status_count) {
          $status_results = $status_query
            ->condition('s.field_ems_status_tid', $booking_status_tid, '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        }
        else {
          // If the field is left empty in the calendar logic term,
          // consider any value as matching.
          $status_results = TRUE;
        }
        if ($event_type_count) {
          $event_type_results = $event_type_query
            ->condition('e.field_ems_booking_type_tid', $booking_event_type_tid, '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        }
        else {
          // If the field is left empty in the calendar logic term,
          // consider any value as matching.
          $event_type_results = TRUE;
        }

        if ($group_type_count) {
          $group_type_results = $group_type_query
            ->condition('g.field_ems_group_type_tid', $booking_group_type_tid, '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        }
        else {
          // If the field is left empty in the calendar logic term,
          // consider any value as matching.
          $group_type_results = TRUE;
        }

        // Set all calendars the booking should show on.
        if ($status_results && $event_type_results && $group_type_results) {
          $node->field_ems_calendar_logic[LANGUAGE_NONE][]['tid'] = $calendar_logic->tid;
        }
      }
    }

    // Count number of calendars a booking should be listed on.
    $calendar_logic_count = 0;
    if (!empty($node->field_ems_calendar_logic[LANGUAGE_NONE])) {
      $calendar_logic_count = count($node->field_ems_calendar_logic[LANGUAGE_NONE]);
    }

    // If node belongs to at least one calendar, then save the node.
    if ($calendar_logic_count > 0) {
      try {
        $node->title = check_plain($booking['EventName']);
        $node->field_ems_booking_id[LANGUAGE_NONE][0]['value'] = $booking['BookingID'];
        $node->field_ems_booking_type[LANGUAGE_NONE][0]['tid'] = $booking_event_type_tid;
        $node->field_ems_status[LANGUAGE_NONE][0]['tid'] = $booking_status_tid;
        $node->field_ems_group_type[LANGUAGE_NONE][0]['tid'] = $booking_group_type_tid;
        $node->field_ems_booking_date[LANGUAGE_NONE][0]['value'] = $booking['TimeBookingStart'];
        $node->field_ems_booking_date[LANGUAGE_NONE][0]['value2'] = $booking['TimeBookingEnd'];
        node_save($node);
        $context['results']['nodes_saved'] = $context['results']['node_saved']++;
      }
      catch (Exception $e) {
        $message = "Saving node with Booking ID " . $booking['BookingID'] . " failed! Error: $e";
        watchdog("EMS API", $message);
      }
    }
    // If node no longer belongs to at least one calendar,
    // and the node exists, delete the node.
    if (($calendar_logic_count == 0) AND ($existing_node_id)) {
      node_delete($existing_node_id);
      $context['results']['nodes_deleted'] = $context['results']['nodes_deleted']++;
    }
  }

  $context['sandbox']['progress'] += $batch_limit;
  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 *
 * @param bool $success
 *   True is batch completes without error.
 * @param array $results
 *   Contains information about batch process success.
 * @param array $operations
 *   Operations still remaining to be processed.
 */
function ems_api_batch_finished($success, $results, $operations) {
  // todo: Add information about term imports.
  if ($success) {
    $message_batch_import = t("!count bookings were processed from EMS.", array(
      '!count' => $results['total_bookings'],
    ));
    $message_batch_process = t("!count nodes were saved/ updated.", array(
      '!count' => $results['nodes_saved'] + 1,
    ));
    drupal_set_message($message_batch_import);
    drupal_set_message($message_batch_process);
    variable_set('ems_api_last_import', REQUEST_TIME);
  }
  else {
    // An error occurred.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing !error_operation with arguments: !arguments', array(
      '!error_operation' => $error_operation[0],
      '!arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
}


/**
 * Checks for existing bookingID, returns the entity_id (node ID) of the event.
 *
 * @param string $booking_id
 *   A $bookingID from EMS is passed and Drupal returns a node ID, if exists.
 *
 * @return string
 *   Returns bookingID, if it exists.
 */
function ems_api_check_booking_exists($booking_id) {

  $value = NULL;
  $db_result = db_select('field_data_field_ems_booking_id', 'f')
    ->fields('f', array('entity_id', 'field_ems_booking_id_value'))
    ->condition('field_ems_booking_id_value', $booking_id, '=')
    ->execute();
  if ($db_result) {
    while ($row = $db_result->fetchAssoc()) {
      $value = $row['entity_id'];
    }
  }
  return $value;
}

/**
 * Returns field mapping for an EMS booking.
 */
function ems_api_booking_field_mapping() {
  return array(
    'AddedBy',
    'AltContact',
    'BookingDate',
    'BookingID',
    'Building',
    'BuildingCode',
    'BuildingID',
    'ChangedBy',
    'ClosedAllDay',
    'CloseTime',
    'Contact',
    'ContactEmailAddress',
    'DateAdded',
    'DateChanged',
    'EventCoordinator',
    'EventName',
    'EventTypeDescription',
    'EventTypeID',
    'GMTEndTime',
    'GMTStartTime',
    'GroupID',
    'GroupName',
    'GroupTypeDescription',
    'GroupTypeID',
    'HVACZone',
    'OpenTime',
    'ReservationID',
    'Room',
    'RoomCode',
    'RoomDescription',
    'RoomID',
    'RoomType',
    'RoomTypeID',
    'SetupCount',
    'SetupTypeDescription',
    'StartBookingDate',
    'StatusID',
    'StatusTypeID',
    'TimeBookingEnd',
    'TimeBookingStart',
    'TimeEventEnd',
    'TimeEventStart',
    'TimeZone',
    'VIP',
    'VIPEvent',
  );
}


/**
 * Imports bookings from EMS.
 *
 * @param string $beginning
 *   Time in Y-m-d format of import windows start.
 * @param string $end
 *   Time in Y-m-d format of import windows end.
 *
 * @return array
 *
 *   Returns array of bookings found within the import window.
 */
function ems_api_bookings_import($beginning, $end) {
  $bookings = new EMS_Get_Bookings(
    variable_get('ems_api_wsdl'),
    variable_get('ems_api_username'),
    variable_get('ems_api_password')
  );

  // Initialize date windows.
  $start_date = $beginning . "T00:00:00";
  $end_date = $end . "T23:59:59";
  // BuildingID of -1 will import all buildings.
  $building_id = -1;
  $view_combo_rooms = FALSE;
  // Retrieve bookings from EMS with given time window.
  $bookings->getAllBookings($start_date, $end_date, $building_id, $view_combo_rooms);
  $xml = $bookings->getXmlResponse();
  $bookings_array = array(array());
  $booking_id = 0;
  foreach ($xml[0]->Data as $ems_api_booking) {
    foreach ($ems_api_booking as $key => $value) {
      if ($key != 'BookingID') {
        $bookings_array[$booking_id][$key] = (string) $value;
      }
      else {
        $bookings_array[$booking_id][$key] = (int) $value;
      }
    }
    $booking_id++;
  }
  return $bookings_array;
}

/**
 * Converts configuration settings from import window to date.
 *
 * @param string $date_setting
 *   Case for date.
 * @param int $offset
 *   Offset variable is used to either find a date in the past or future.
 *
 * @return string
 *   Current date +/- offset in Y-m-d format.
 */
function ems_api_convert_import_date($date_setting, $offset) {
  switch ($date_setting) {
    case "Week":
      $date = date('Y-m-d', (time() + ((60 * 60 * 24 * 7) * $offset)));
      break;

    case "Month":
      $date = date('Y-m-d', (time() + ((60 * 60 * 24 * 31) * $offset)));
      break;

    case "Half Year":
      $date = date('Y-m-d', (time() + ((60 * 60 * 24 * 183) * $offset)));
      break;

    case "Year":
      $date = date('Y-m-d', (time() + ((60 * 60 * 24 * 366) * $offset)));
      break;

    default:
      // Consider anything else to mean just today.
      $date = date('Y-m-d');
  }
  return $date;
}

/**
 * Writes booking results to the database.
 */
function ems_api_export_to_db($booking) {
  $ems_api_import_error = NULL;
  $fields = ems_api_booking_field_mapping();
  // Removes empty array elements to avoid db_insert warnings.
  foreach ($fields as $key => $element) {
    if (empty($booking[$element])) {
      unset($fields[$key]);
      unset($booking[$key]);
    }
  }

  $query = db_insert('ems_api_all_bookings_result')
    ->fields($fields);
  $query->values($booking);

  try {
    $query->execute();
  }
  catch (Exception $ems_api_import_error) {
    watchdog("EMS API", "GetAllBookingsResult Insert Query Failed");
    watchdog("EMS API", $ems_api_import_error);
  }
  return $ems_api_import_error;
}

/**
 * Imports EMS EventType to Taxonomy.
 */
function ems_api_eventtype_import() {
  watchdog('EMS API Event', 'Beginning ems_booking_types import');
  $vocab = taxonomy_vocabulary_machine_name_load('ems_booking_types');
  $final_key = NULL;
  $final_value = NULL;

  if (!empty($vocab)) {

    $event_type = new EMS_GetEventTypes(
      variable_get('ems_api_wsdl'),
      variable_get('ems_api_username'),
      variable_get('ems_api_password')
    );
    $event_type->getEventTypes();
    $xml = $event_type->getXmlResponse();
    $events = array();
    // Creates array of ID => EventType descriptions.
    foreach ($xml[0]->Data as $key1 => $value1) {
      foreach ($value1 as $key2 => $data2) {
        if ($key2 == "ID") {
          $final_key = (int) $data2;
        }
        elseif ($key2 == "Description") {
          $final_value = (string) $data2;
        }
        if ($final_key && $final_value) {
          $events[$final_value] = $final_key;
        }
      }
    }

    // Save array of ID => Status Descriptions to term.
    foreach ($events as $event_type => $event_type_id) {

      $term = (object) array(
        'name' => $event_type,
        'description' => $event_type_id,
        'vid' => $vocab->vid,
        'tid' => get_tid_from_description($event_type_id, 'ems_booking_types'),
      );
      taxonomy_term_save($term);
    }
    watchdog('EMS API Event', 'Finished ems_booking_types import');
  }
  else {
    watchdog('EMS API Event', 'No taxonomy for ems_booking_types found');
  }
}

/**
 * Imports EMS GroupType to Taxonomy.
 */
function ems_api_grouptype_import() {
  $vocab = taxonomy_vocabulary_machine_name_load('ems_group_types');
  $final_key = NULL;
  $final_value = NULL;
  watchdog('EMS API Groups', 'Begin Group Type Import');
  if (!empty($vocab)) {
    $status = new EMS_Get_Group_Types(
      variable_get('ems_api_wsdl'),
      variable_get('ems_api_username'),
      variable_get('ems_api_password')
    );
    $status->getGroupTypes();
    $xml = $status->getXmlResponse();
    $groups = array();
    // Creates array of ID => Status descriptions.
    foreach ($xml[0]->Data as $key1 => $value1) {
      foreach ($value1 as $key2 => $data2) {

        if ($key2 == "ID") {
          $final_key = (int) $data2;
        }
        elseif ($key2 == "Description") {
          $final_value = (string) $data2;
        }
        if ($final_key && $final_value) {
          $groups[$final_value] = $final_key;
          $final_key = NULL;
          $final_value = NULL;
        }
      }
    }

    // Save array of ID => Status Descriptions to term.
    foreach ($groups as $group_type => $group_id) {
      $term = (object) array(
        'name' => $group_type,
        'description' => $group_id,
        'vid' => $vocab->vid,
        'tid' => get_tid_from_description($group_id, 'ems_group_types'),
      );
      // Don't import the "(none)" group type.
      if ($group_id != 0) {
        taxonomy_term_save($term);
      }
    }
    watchdog('EMS API', 'Finished ems_group_types import');
  }
  else {
    watchdog('EMS API', 'No taxonomy for ems_group_types found');
  }
}

/**
 * Imports EMS StatusID to Taxonomy.
 */
function ems_api_statusid_import() {
  watchdog('EMS API Statuses', 'Begin Statuses Import');
  $vocab = taxonomy_vocabulary_machine_name_load('ems_statuses');
  $final_key = NULL;
  $final_value = NULL;

  if (!empty($vocab)) {
    $status = new EMS_Get_Statuses(
      variable_get('ems_api_wsdl'),
      variable_get('ems_api_username'),
      variable_get('ems_api_password')
    );
    $status->getStatuses();
    $xml = $status->getXmlResponse();
    $statuses = array();
    // Creates array of ID => Status descriptions.
    foreach ($xml[0]->Data as $key1 => $value1) {
      foreach ($value1 as $key2 => $data2) {
        if ($key2 == "ID") {
          $final_key = (int) $data2;
        }
        elseif ($key2 == "Description") {
          $final_value = (string) $data2;
        }
        if ($final_key && $final_value) {
          $statuses[$final_value] = $final_key;
          $final_key = NULL;
          $final_value = NULL;
        }
      }
    }

    // Save array of ID => Status Descriptions to term.
    foreach ($statuses as $status => $status_id) {

      $term = (object) array(
        'name' => $status,
        'description' => $status_id,
        'vid' => $vocab->vid,
        'tid' => get_tid_from_description($status_id, 'ems_statuses'),
      );
      taxonomy_term_save($term);
    }
    watchdog('EMS API Statuses', 'Finished ems_statuses import');
  }
  else {
    watchdog('EMS API Statuses', 'No taxonomy for ems_statuses found');
  }
}

/**
 * Finds a taxonomy term by name.
 *
 * @param string $term_description
 *   Description to search for.
 * @param string $vocabulary_name
 *   Taxonomy term to search.
 *
 * @return int
 *   If match is found, return tid.
 */
function get_tid_from_description($term_description, $vocabulary_name) {
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name)) {
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term) {
      if ($term->description == $term_description) {
        return $term->tid;
      }
    }
  }
  return NULL;
}
