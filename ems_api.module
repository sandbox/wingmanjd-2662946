<?php

/**
 * @file
 * Connects Drupal and Dean Evan's and Associates, LLC EMS application.
 *
 * The EMS API module connects Drupal with Dean Evan's and Associates, LLC EMS
 * application and imports events as Drupal nodes based on user-defined
 * configurations.
 *
 * @author wingmanjd
 */

include_once 'ems_api.batch.inc';

/**
 * Implements hook_permission().
 */
function ems_api_permission() {

  return array(
    'administer ems api module' => array(
      'title' => t('Administer EMS API module'),
      'description' => t('Perform administration tasks for the EMS API module.'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * Creates settings form for EMS WSDL and API credentials.
 */
function ems_api_menu() {
  $items = array();
  $items['admin/config/services/ems_api_connector_settings'] = array(
    'title' => 'EMS API',
    'description' => 'Configure EMS API settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ems_api_settings_form'),
    'access arguments' => array('administer ems api module'),
    'file' => 'forms/ems_api_settings_form.inc',
  );
  $items['admin/content/ems_api_connector_import'] = array(
    'title' => 'Import EMS API data',
    'description' => 'Imports data from EMS API to EMS Event Content Type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ems_api_import_form'),
    'access arguments' => array('administer ems api module'),
    'file' => 'forms/ems_api_import_form.inc',
  );
  return $items;
}

/**
 * Prepare ems_api import batch definition.
 */
function ems_api_batch() {
  $batch = array(
    'operations' => array(
      // Imports data from EMS to temporary table.
      array('ems_api_batch_import', array()),
      // Processes temporary table data to Drupal nodes.
      array('ems_api_batch_process', array()),
    ),
    'finished' => 'ems_api_batch_finished',
    'title' => t('Processing EMS API import'),
    'init_message' => t('Starting EMS API import'),
    'progress_message' => 'Importing events from ' . variable_get('ems_api_wsdl'),
    'error_message' => t('EMS API import has encountered an error.'),
    'file' => drupal_get_path('module', 'ems_api') . 'ems_api.batch.inc',
  );
  return $batch;
}

/**
 * Run the ems_api import.
 */
function ems_api_import() {
  $ems_batch = ems_api_batch();
  batch_set($ems_batch);

  $ems_batch =& batch_get();
  // Setting the batch as progressive allows us to run it on cron.
  $ems_batch['progressive'] = FALSE;

  watchdog('EMS API', 'Beginning automated EMS API import.');
  // Set this variable when we start the migration so the value is stored and
  // we don't run another process over it.
  variable_set('ems_api_last_import', REQUEST_TIME);
  batch_process();
  watchdog('EMS API', 'Finished automated EMS API import.');
}

/**
 * Implements hook_cron().
 *
 * Limits ems cron from running more than once per day.
 */
function ems_api_cron() {
  $last_run = variable_get('ems_api_last_import');
  $offset = strtotime('today 5:00 am');
  $today = strtotime('today midnight');

  // Check when ems_api_cron ran last.
  if ($today > $last_run) {
    if (REQUEST_TIME > $offset) {
      ems_api_import();
    }
  }

}
