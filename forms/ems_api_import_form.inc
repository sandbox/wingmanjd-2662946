<?php

/**
 * @file
 * Manually initiates an import from EMS API.
 */
function ems_api_import_form() {
  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Running this process will create and update EMS Event nodes from EMS API data.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Begin Manual Import',
  );
  return $form;
}

/**
 * Import EMS data form submission.
 */
function ems_api_import_form_submit($form, &$form_state) {
  $ems_batch = ems_api_batch();
  watchdog('EMS API', 'Beginning manual import.');
  variable_set('ems_api_last_import', REQUEST_TIME);
  batch_set($ems_batch);
}
