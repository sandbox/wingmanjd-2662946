<?php

/**
 * @file
 * Configuration form for ems_api module.
 */

/**
 *
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function ems_api_settings_form($form, &$form_state) {

  $form = array();
  $form['ems_api_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('EMS API WSDL URL'),
    '#default_value' => variable_get('ems_api_wsdl'),
    '#description' => t('Full URL for the EMS API WSDL (i.e. https://site.tld/EMSAPI/Service.asmx?WSDL)'),
    '#required' => TRUE,
  );
  $form['ems_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('EMS API Username'),
    '#default_value' => variable_get('ems_api_username'),
    '#description' => t('EMS API UserName'),
    '#required' => TRUE,
  );
  $form['ems_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('EMS API Password'),
    '#default_value' => variable_get('ems_api_password'),
    '#description' => t('EMS API Password'),
    '#required' => TRUE,
  );
  $form['ems_beginning_import_date_window'] = array(
    '#type' => 'select',
    '#title' => t('Beginning Date to import'),
    '#options' => array(
      'Select' => 'Select a date range',
      'Today' => 'Today',
      'Week' => '-1 Week',
      'Month' => '-1 Month',
      'Half Year' => '-6 Months',
      'Year' => '-1 Year',
    ),
    '#default_value' => variable_get('ems_beginning_import_date_window'),
  );
  $form['ems_end_import_date_window'] = array(
    '#type'          => 'select',
    '#title'         => t('End Date to import'),
    '#options'       => array(
      'Select' => 'Select a date range',
      'Week' => '+1 Week',
      'Month' => '+1 Month',
      'Half Year' => '+6 Months',
      'Year' => '+1 Year',
    ),
    '#default_value' => variable_get('ems_end_import_date_window'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#validate'][] = 'ems_api_validate_settings';

  return (system_settings_form($form));
}


/**
 * @param $form
 * @param $form_state
 */
function ems_api_validate_settings($form, &$form_state) {

  // todo: use form_state to validate.
  // dpm($form_state);
  // todo: use form_state password, if blank, use variable_get('ems_api_password')
  // todo: form_set_error on each field to be checked.
}
