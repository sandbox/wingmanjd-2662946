<?php
/**
 * @file
 * Base EMS API class.
 */

/**
 * Base EMS API Call class.
 */
class EmsApiCall {

  /**
   * Properties.
   */
  protected $wsdl;
  protected $soapParameters = array();
  protected $xmlResponse;

  /**
   * Constructor.
   */
  public function __construct($wsdl, $username, $password) {
    $this->wsdl = $wsdl;
    $this->soapParameters['UserName'] = $username;
    $this->soapParameters['Password'] = $password;
  }

  /**
   * Retrieves $this->wsdl property.
   *
   * @return string mixed
   *   URL of the WSDL.
   */
  public function getWsdl() {
    return $this->wsdl;
  }

  /**
   * Sets $this->wsdl property.
   *
   * @param string $wsdl
   *   URL of the WSDL.
   */
  public function setWsdl($wsdl) {
    $this->wsdl = $wsdl;
  }

  /**
   * @return array
   */
  public function getSoapParameters() {
    return $this->soapParameters;
  }

  /**
   * @param array $soapParameters
   */
  public function setSoapParameters($soapParameters) {
    $this->soapParameters = $soapParameters;
  }

  /**
   * @return mixed
   */
  public function getXmlResponse() {
    return $this->xmlResponse;
  }

  /**
   * @param mixed $xmlResponse
   */
  public function setXmlResponse($xmlResponse) {
    $this->xmlResponse = $xmlResponse;
  }

}
