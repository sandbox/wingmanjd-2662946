<?php

/**
 * Created by PhpStorm.
 * User: jgonyea_admin
 * Date: 7/27/2015
 * Time: 2:02 PM
 */

class EMS_Get_Bookings extends EmsApiCall {

  public function getAllBookings($start_date, $end_date, $building_id, $view_combo_rooms){
    $client = new SoapClient($this->wsdl, array('trace' => 1));
    $this->soapParameters['StartDate'] = $start_date;
    $this->soapParameters['EndDate'] = $end_date;
    $this->soapParameters['BuildingID'] = $building_id;
    $this->soapParameters['ViewComboRoomComponents'] = $view_combo_rooms;
    $this->xmlResponse = simplexml_load_string(
      $client
        ->GetAllBookings($this->soapParameters)
        ->GetAllBookingsResult
    );
  }
}