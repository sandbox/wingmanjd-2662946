<?php
/**
 * Created by PhpStorm.
 * User: jgonyea_admin
 * Date: 7/27/2015
 * Time: 2:02 PM
 */

class EMS_GetEventTypes extends EmsApiCall {
  public function getEventTypes(){
    $client = new SoapClient($this->wsdl, array('trace' => 1));
    $this->xmlResponse = simplexml_load_string(
      $client
        ->GetEventTypes($this->soapParameters)
        ->GetEventTypesResult
    );
  }
}