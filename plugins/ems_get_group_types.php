<?php
/**
 * Created by PhpStorm.
 * User: jgonyea_admin
 * Date: 7/27/2015
 * Time: 2:40 PM
 */

class EMS_Get_Group_Types extends EmsApiCall{
  public function getGroupTypes(){
    $client = new SoapClient($this->wsdl, array('trace' => 1));
    $this->xmlResponse = simplexml_load_string(
      $client
        ->GetGroupTypes($this->soapParameters)
        ->GetGroupTypesResult
    );
  }
}