<?php
/**
 * @file
 * Imports status types from EMS.
 */

/**
 * Class that holds logic for an EMS GetStatuses call.
 */
class EMSGetStatuses extends EmsApiCall {

  /**
   * EMS API GetStatuses call.
   */
  public function getStatuses() {
    $client = new SoapClient($this->wsdl, array('trace' => 1));
    $this->xmlResponse = simplexml_load_string(
      $client
        ->GetStatuses($this->soapParameters)
        ->GetStatusesResult
    );
  }

}
